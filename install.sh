prefix="\e[95m[INSTALL]\e[39m"

echo -e "$prefix Install git..."
apt-get install -y git

echo -e "$prefix Clone the private setup files..."
git clone https://m0rv4i@bitbucket.org/m0rv4i/linux-setup.git /tmp/linux-setup

echo -e "$prefix Execute the private install script..."
chmod u+x /tmp/linux-setup/install.sh
bash -c "/tmp/linux-setup/install.sh" | tee ~/linux-setup-install.log



